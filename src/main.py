from lib import messages_handler
from lib import keyboard
from lib import data_handler
from secret import token


from telepot.loop import MessageLoop
import time
import telepot
# import sys

TOKEN = token.get_token()

bot = telepot.Bot(TOKEN)
msgHandler = messages_handler.handler()
current_chat = {'id': None}

def handle_new_message(msg):
  content_type, chat_type, chat_id = telepot.glance(msg)
  current_chat['id'] = chat_id

  if (content_type == 'text'):
    if (not msg['text'] == "/start"):
      result = msgHandler.handle(str(msg['text']), bot, chat_id)
      if (not result == None):
        board = keyboard.loadKeyboard()
        bot.sendMessage(chat_id, "Seleziona", reply_markup=board)
        


def handle_callback(msg):
  query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
  query = query_data.split("@")
  print(query[0], query[1])

def initializeBot():
  MessageLoop(bot, {'chat': handle_new_message, 'callback_query': handle_callback}).run_as_thread()
  print('Listening ...')
  data = data_handler.patient_handler()
  data.load_patients()
  
  while True:
    time.sleep(10)
    data.generate_data()
    crit = data.testfor_critical()
    for c in crit:
      cp = data.get_patient_by_id(c['id'])
      if (not cp == None):
        if (not current_chat['id'] == None):
          tosend = "Critical patient!!!\n%s %s has a 'spO2' value of %s" % (cp['surname'], cp['name'], c['value'])
          msgHandler.cloneMex(tosend, bot, current_chat['id'])
        pass


initializeBot()
