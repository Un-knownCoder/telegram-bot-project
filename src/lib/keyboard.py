from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

def loadKeyboard():
  board = InlineKeyboardMarkup(inline_keyboard=[
    [
      InlineKeyboardButton(text='Press me', callback_data='user-details@Vettori Massimo')
    ],
  ])

  return board
