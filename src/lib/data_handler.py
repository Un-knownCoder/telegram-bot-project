import random
import time


class patient_handler:
  def __init__(self):
    self.patients = []
    self.patient_data = []
  
  def load_patients(self):
    try:
      data = open('./temp/data','r').read().split('\n')
      for p in data:
        tmp = p.split('#')
        if(len(tmp) == 4):
          self.patients.append( {"id": tmp[0], "surname": tmp[1], "name": tmp[2], "address": tmp[3]} )

    except Exception as e:
      print("Fatal data fetch - Aborting execution", e)
      exit(1)

  def generate_data(self):
    self.patient_data.clear()
    for i in range(0, len(self.patients)):
      spo2 = random.randint(85, 100)
      fc = random.randint(0, 100)
      fr = random.randint(0, 100)
      pi = random.randint(0, 100)

      self.patient_data.append({"pid": self.patients[i]['id'], "spO2": spo2, "FC": fc, "FR": fr, "PI": pi, "time": time.strftime("%H:%M:%S", time.localtime())})

  def testfor_critical(self):
    critical_list = []
    for i in range(0, len(self.patient_data)):
      if (self.patient_data[i]['spO2'] <= 92):
        critical_list.append({'id': self.patient_data[i]['pid'], 'value': self.patient_data[i]['spO2']})

    return critical_list

  def get_patient_by_id(self, id): 
    for p in self.patients:
      if (p['id'] == id):
        return p